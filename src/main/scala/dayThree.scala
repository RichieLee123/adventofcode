object dayThree extends adventOfCodeApp {
  val map = resourceForDay(3).zipWithIndex

  val upperLimit = map.head._1.length

  def traverse(right: Int, down: Int): Int = {
    val res = map.foldLeft((0,0)){ (countAndPos,line) => {
      val count = countAndPos._1
      val pos = countAndPos._2

      if (line._2 % down != 0) (count, pos)
      else
        line._1(pos) match {
          case '#' => (count+1, (pos+right) % upperLimit)
          case _ => (count, (pos+right) % upperLimit)
        }
      }
    }
    res._1
  }

  val r1d1: BigInt = traverse(1,1)
  val r3d1: BigInt = traverse(3,1)
  val r5d1: BigInt = traverse(5,1)
  val r7d1: BigInt = traverse(7,1)
  val r1d2: BigInt = traverse(1,2)

  print(r1d1 * r3d1 * r5d1 * r7d1 * r1d2)
}
