import scala.io.Source

trait adventOfCodeApp extends App {
  def resourceForDay(day: Int): List[String] = Source.fromResource(s"day$day.txt").getLines().toList
}
