import java.lang.Integer.parseInt

case class Rule(min: Int, max: Int, letter: Char)
case class PasswordAndRule(
  password: String,
  rule: Rule
) {
  def passesFirstRule: Boolean = rule.min to rule.max contains password.count(_ == rule.letter)
  def passesSecondRule: Boolean = {
    val first = password(rule.min - 1)
    val second = password(rule.max - 1)

    (!first.equals(second)) && (second.equals(rule.letter) || first.equals(rule.letter))
  }
}

object dayTwo extends adventOfCodeApp {
  def parseLine(line: String): PasswordAndRule = {
    val rawRuleAndPassword = line.split(":")
    PasswordAndRule(rawRuleAndPassword(1).trim, parseRule(rawRuleAndPassword(0)))
  }

  def parseRule(str: String): Rule = {
    val rules = str.split(" ")(0).split("-").map(parseInt)
    Rule(rules(0), rules(1), str.last)
  }

  val passwordData = resourceForDay(2).map(parseLine)

  println(s"""part one: ${passwordData.count(_.passesFirstRule)}""")
  println(s"""part two: ${passwordData.count(_.passesSecondRule)}""")
}
