import java.lang.Integer.parseInt

object dayOne extends adventOfCodeApp {
  def sum2020(list: List[Int]): (Int,Int) =
    list.tail.find(list.head + _ == 2020).map((list.head, _)).getOrElse{
      if (list.tail.size < 2) throw new RuntimeException("list too small")
      else sum2020(list.tail)
    }

  def sum2020WithThree(list: List[Int]): (Int,Int,Int) = {
    if (list.size < 3) throw new RuntimeException("list too small")
    val first = list.head
    val rest = list.tail.flatMap(e => list.tail.tail.map(f => ((f, e), e+f)))

    rest.find(e => e._2 + first == 2020).map(e => (first, e._1._1, e._1._2)).getOrElse(sum2020WithThree(list.tail))
  }

  val expenses = resourceForDay(1).map(parseInt)
  val res1 = sum2020(expenses)
  val res2 = sum2020WithThree(expenses)
  println(res1._2 * res1._1)
  println(res2._2 * res2._1 * res2._3)
}
